package com.example.ultimate_ttt

import android.util.Log

/**
 * Processes the move a player made and checks the board for winners
 *
 * @property board is initialised after the move of a player has been processed
 * @property turn show which players turn it is
 *
 */
class Moves {
    private lateinit var board: Board
    private var turn = Box.Companion.State.Player1.name

    /**
     * First processes the move of a player and sets the state of the box, then calls the check
     * function the check if the board was won by a player or a draw and changes it's state also
     * accordingly
     *
     * @param board the board in which a move was made. It can be a InnerBoard or an OuterBoard
     * element
     * @param box the box that was clicked. If a OuterBoard element is given in the board param.,
     * this box param. must be null
     */
    fun processTurnBox(board: Board, box: Box?): Boolean? {
        if (box != null) {
            if (box.getBoxState() != Box.Companion.State.Empty.name || board.getBoxState() != Box.Companion.State.Empty.name) {
                return null
            }
            if (turn == Box.Companion.State.Player1.name) {
                box.setWinner(true)
            } else {
                box.setWinner(false)
            }
        }
        this.board = board
        val winner = this.checkBoard()
        Log.v("BoardType", this.board::class.java.typeName)
        if (this.board is OuterBoard) {
            board.items.values.forEach {
                Log.v("colOuterBoard", it.getBoxState())
            }
        }
        return when(winner){
            Box.Companion.State.Player1.name -> {board.setWinner(true); true
            }

            Box.Companion.State.Player2.name -> {board.setWinner(false); true
            }

            Box.Companion.State.Draw.name -> {board.draw(); if(this.board is InnerBoard) {this.changeTurn()}; false
            }

            Box.Companion.State.Empty.name -> {this.changeTurn(); false
            }

            else -> false
        }
    }


    /**
     * Changes which players turn it is
     */
    private fun changeTurn() {
        turn = if (turn == Box.Companion.State.Player1.name) {
            Box.Companion.State.Player2.name
        } else {
            Box.Companion.State.Player1.name
        }

    }

    fun getCurrentTurn(): String {
        return this.turn
    }

    /**
     *  Checks the board for a winner. Works with InnerBoard and also OuterBoard elements, because
     *  they all are also Box elements which holds the exact same states and all functions the Box
     *  class has
     *
     *  If there are less then 3 Box (InnerBoard) elements not empty (or a draw) the checkBoard
     *  function returns the Empty state
     *
     *  If all elements of the board are filled and there is still no decisive winner it returns
     *  the Draw state
     *
     *  If one check-functions returns the value 'true', this 'checkBoard' function immediately
     *  finishes without running the other check functions that would come after and returns the
     *  player which current turn it is to mark him as the winner
     *
     */
    private fun checkBoard(): String {
        if (this.board.getCount() < 3) {
            Log.v("CheckBoard", "SkipCheck!!!!!: ${this.board.getCount()}")
            return Box.Companion.State.Empty.name
        }

        val winnerRow = this.checkRows()
        if (winnerRow) {
            return turn
        }
        val winnerCol = this.checkColumns()
        if (winnerCol) {
            return turn
        }
        val winnerDia = this.checkDiagonal()
        if (winnerDia) {
            return turn
        }
        if(this.board.getCount() == 9) {
            return Box.Companion.State.Draw.name
        }
        return Box.Companion.State.Empty.name
    }

    /**
     * Checks each row of the board. Returns true if there is a winner
     */
    private fun checkRows(): Boolean {
        for (i in 0..2) {
            val firstinrow: String = this.board.items["${i}0"]!!.getBoxState()
            if ((firstinrow == Box.Companion.State.Player1.name) or
                (firstinrow == Box.Companion.State.Player2.name)
            ) {
                if ((firstinrow == this.board.items["${i}1"]!!.getBoxState()) and
                    (firstinrow == this.board.items["${i}2"]!!.getBoxState())
                ) {
                    Log.v("RowInnerBoardWinner", "RowWinner: $firstinrow")
                    return true
                }
            } else {
                continue
            }
        }
        return false
    }

    /**
     * Checks each column of the board. Returns true if there is a winner
     */
    private fun checkColumns(): Boolean {
        for (i in 0..2) {
            val firstincolumn: String = this.board.items["0${i}"]!!.getBoxState()
            if ((firstincolumn == Box.Companion.State.Player1.name) or
                (firstincolumn == Box.Companion.State.Player2.name)
            ) {
                if ((firstincolumn == this.board.items["1${i}"]!!.getBoxState()) and
                    (firstincolumn == this.board.items["2${i}"]!!.getBoxState())
                ) {
                    Log.v("ColInnerBoardWinner", "ColWinner: $firstincolumn")
                    return true
                }
            } else {
                continue
            }
        }
        return false
    }

    /**
     * Checks each Diagonal of the board. Returns true if there is a winner
     */
    private fun checkDiagonal(): Boolean {
        for (i in 0..2 step 2) {
            val firstindiagnoal: String = this.board.items["0${i}"]!!.getBoxState()
            if ((firstindiagnoal == Box.Companion.State.Player1.name) or
                (firstindiagnoal == Box.Companion.State.Player2.name)
            ) {
                if ((firstindiagnoal == this.board.items["11"]!!.getBoxState()) and
                    (firstindiagnoal == this.board.items["2${2-i}"]!!.getBoxState())
                ) {
                    Log.v("DiaInnerBoardWinner", "DiaWinner: $firstindiagnoal")
                    return true
                }
            } else {
                continue
            }
        }
        return false
    }
}
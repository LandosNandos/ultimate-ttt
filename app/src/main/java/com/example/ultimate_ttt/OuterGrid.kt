package com.example.ultimate_ttt

import android.app.AlertDialog
import android.os.Bundle
import android.util.TypedValue
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.GridLayout
import androidx.activity.OnBackPressedCallback
import androidx.fragment.app.activityViewModels

class OuterGrid : Fragment() {
    private val gridKeys = arrayOf("00", "01", "02", "10", "11", "12", "20", "21", "22")
    private val gameLogic: GameLogic by activityViewModels()
    private val boardTextHash = arrayOf(0, 3, 6, 9, 12, 15, 18, 21, 24)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_outer_grid, container, false)
        val gridLayout = view.findViewById<GridLayout>(R.id.gridLayout)
        val outerBoard = gameLogic.getOuterBoard()
        if (outerBoard.getBoxState() != "Empty") {
            (requireActivity() as MainActivity).displayWinnerText(outerBoard)
        }
        setOuterBoardView(gridLayout, outerBoard)
        val callback = object : OnBackPressedCallback(true) {
            override fun handleOnBackPressed() {
                showExitConfirmationDialog()
            }
        }
        requireActivity().onBackPressedDispatcher.addCallback(viewLifecycleOwner, callback)

        return view
    }

    private fun setOuterBoardView(gridLayout: GridLayout, outerBoard: OuterBoard) {
        var lastClickedBox = gameLogic.getLastClickedBoardIndex()
        var solvedInnerBoard = outerBoard.items[gridKeys[0]] as InnerBoard
        if (lastClickedBox < 9) {
            solvedInnerBoard = outerBoard.items[gridKeys[lastClickedBox]] as InnerBoard
        }
        for (i in 0 until gridLayout.childCount) {
            val button = gridLayout.getChildAt(i) as Button
            val innerBoard = outerBoard.items[gridKeys[i]] as InnerBoard
            val test = innerBoard.getBoxState()
            if (solvedInnerBoard.getBoxState() != "Empty") {
                lastClickedBox = 9
            }
            if ((lastClickedBox < 9 && i != lastClickedBox) || innerBoard.getBoxState() != "Empty") {
                button.alpha = 0.8f
                button.isEnabled = false
            }
            button.setOnClickListener {
                gameLogic.onInnerBoardClick(gridKeys[i])!!
                (requireActivity() as MainActivity).switchToInnerGridFragment(InnerGrid())
            }
            setInnerBoardView(innerBoard, button)
        }
    }

    private fun setInnerBoardView(innerBoard: InnerBoard, button: Button) {
        var newText = "  |  |  \n  |  |  \n  |  |  "
        var newSizeInSp = 14f
        if (innerBoard.getBoxState() == "Player1") {
            newText = "X"
            newSizeInSp = 30f
        } else if (innerBoard.getBoxState() == "Player2") {
            newText = "O"
            newSizeInSp = 30f
        } else if (innerBoard.getBoxState() == "Draw") {
            newText = "-"
            newSizeInSp = 30f
        } else {
            for (i in 0 until innerBoard.items.size) {
                if (innerBoard.items[gridKeys[i]]!!.getBoxState() ==
                    Box.Companion.State.Player1.name
                ) {
                    newText = replaceNthSpace(newText, i, 'X')
                } else if (innerBoard.items[gridKeys[i]]!!.getBoxState() ==
                    Box.Companion.State.Player2.name
                ) {
                    newText = replaceNthSpace(newText, i, 'O')
                }
            }
        }
        button.text = newText
        button.setTextSize(TypedValue.COMPLEX_UNIT_SP, newSizeInSp)
    }

    private fun replaceNthSpace(input: CharSequence, n: Int, replacement: Char): String {
        val stringBuilder = StringBuilder(input)
        val dif = 26-input.length
        stringBuilder.setCharAt(boardTextHash[n]-dif, replacement)
        stringBuilder.deleteCharAt(boardTextHash[n]-dif+1)
        return stringBuilder.toString()
    }

    private fun showExitConfirmationDialog() {
        val builder = AlertDialog.Builder(requireContext())
        builder.setTitle("Achtung")
        builder.setMessage("Möchten Sie das Spiel wirklich verlassen? Der Spielfortschritt wird nicht gespeichert!")
        builder.setPositiveButton("Ja") { _, _ ->
            requireActivity().finish()
        }
        builder.setNegativeButton("Nein") { dialog, _ ->
            dialog.dismiss()
        }
        builder.show()
    }
}
package com.example.ultimate_ttt

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.GridLayout
import android.widget.TextView
import androidx.fragment.app.activityViewModels

class InnerGrid : Fragment() {
    private lateinit var textView: TextView
    private val gameLogic: GameLogic by activityViewModels()
    private val locationText = mutableMapOf(
        "00" to "Upper Left Board",
        "01" to "Upper Middle Board",
        "02" to "Upper Right Board",
        "10" to "Middle Left Board",
        "11" to "True Middle Board",
        "12" to "Middle Right Board",
        "20" to "Bottom Left Board",
        "21" to "Bottom Middle Board",
        "22" to "Bottom Right Board")

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_inner_grid, container, false)
        val gridLayout = view.findViewById<GridLayout>(R.id.gridLayout)
        val key = gameLogic.getLastClickedBoardIndexStr()
        val innerBoard = gameLogic.getInnerBoard()
        textView = view.findViewById(R.id.textView)
        textView.text = locationText[key]
        buttonsForGridBoxes(gridLayout)
        setState(gridLayout, innerBoard)

        return view
    }

    private fun setState(gridLayout: GridLayout, innerBoard: MutableList<String>) {
        for (i in 0 until gridLayout.childCount) {
            val button = gridLayout.getChildAt(i) as Button
            if (innerBoard[i] == Box.Companion.State.Player1.name) {
                button.text = "X"
            } else if (innerBoard[i] == Box.Companion.State.Player2.name) {
                button.text = "O"
            }
        }
    }

    private fun buttonsForGridBoxes(gridLayout: GridLayout) {
        for (i in 0 until gridLayout.childCount) {
            val key = locationText.keys.elementAt(i)
            val button = gridLayout.getChildAt(i) as Button
            button.setOnClickListener {
                val turnProcessed = gameLogic.onBoxClick(key)
                val innerBoard = gameLogic.getInnerBoard()
                setState(gridLayout, innerBoard)
                afterButtonClicked(gridLayout)
                (requireActivity() as MainActivity).setTurnText()
                if(turnProcessed) {
                    gameLogic.setLastClickedBoardIndex(i)
                }
            }
        }
    }

    private fun afterButtonClicked(gridLayout: GridLayout) {
        val backButton = view?.findViewById<Button>(R.id.backButton)
        if (backButton != null) {
            backButton.visibility = View.VISIBLE
            backButton.setOnClickListener {
                requireActivity().onBackPressedDispatcher.onBackPressed()
            }
        }
        for (i in 0 until gridLayout.childCount) {
            val button = gridLayout.getChildAt(i) as Button
            button.alpha = 0.8f
            button.isEnabled = false
        }
    }
}
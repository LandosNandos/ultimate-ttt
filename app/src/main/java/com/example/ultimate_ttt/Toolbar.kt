package com.example.ultimate_ttt

import android.annotation.SuppressLint
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar

class Toolbar : Fragment() {

    private var activityText: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            activityText = it.getString("selectedActivity")
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_toolbar, container, false)
        val toolbar: View? = view.findViewById(R.id.toolbar)

        (requireActivity() as AppCompatActivity).setSupportActionBar(toolbar as Toolbar?)
        (requireActivity() as AppCompatActivity).supportActionBar?.apply {
            setDisplayHomeAsUpEnabled(true)
            setDisplayShowHomeEnabled(true)
            title = activityText
        }

        toolbar?.setNavigationOnClickListener {
            requireActivity().onBackPressedDispatcher.onBackPressed()
        }

        return view
    }

    companion object {
        fun newInstance(yourString: String): com.example.ultimate_ttt.Toolbar {
            val fragment = Toolbar()
            val args = Bundle()
            args.putString("selectedActivity", yourString)
            fragment.arguments = args
            return fragment
        }
    }
}
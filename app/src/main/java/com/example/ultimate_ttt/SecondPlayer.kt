package com.example.ultimate_ttt

/**
 * Interface for every complex SecondPlayer like KI or an other Android Device
 */
interface SecondPlayer {
    fun processTurn()
}
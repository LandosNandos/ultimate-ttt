package com.example.ultimate_ttt

import android.app.AlertDialog
import android.os.Bundle
import android.widget.TextView
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment

class MainActivity : AppCompatActivity() {

    private val gameLogic: GameLogic by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val activityText = intent.getStringExtra("selectedActivity")
        val toolbarFragment = activityText?.let { Toolbar.newInstance(it) }

        if (toolbarFragment != null) {
            addFragmentToActivity(toolbarFragment, R.id.toolbarContainer)
        }
        addFragmentToActivity(OuterGrid(), R.id.gridContainer)
        gameLogic.simpleSecondPlayer()

        setTurnText()
    }

    fun setTurnText() {
        val turnText = findViewById(R.id.turnText) as TextView
        turnText.text = "Current Turn: \n" + gameLogic.getCurrentTurn()
    }

    fun displayWinnerText(outerBoard: OuterBoard) {
        val builder = AlertDialog.Builder(this)
        if (outerBoard.getBoxState() == "Player1") {
            builder.setTitle("Player 1 Wins!")
        } else if (outerBoard.getBoxState() == "Player2") {
            builder.setTitle("Player 2 Wins!")
        } else {
            builder.setTitle("Draw")
        }
        builder.setPositiveButton("OK") { _, _ ->
            finish()
        }

        val alertDialog: AlertDialog = builder.create()
        alertDialog.show()
    }

    private fun addFragmentToActivity(fragment: Fragment, id: Int){
        supportFragmentManager.beginTransaction()
            .replace(id, fragment)
            .commit()
    }

    fun switchToInnerGridFragment(fragment: InnerGrid?) {
        if (fragment == null) return
        val innerGridFragment = InnerGrid()
        supportFragmentManager.beginTransaction()
            .replace(R.id.gridContainer, innerGridFragment)
            .addToBackStack(null)
            .commit()
    }
}
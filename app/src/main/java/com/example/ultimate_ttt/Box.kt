package com.example.ultimate_ttt

/**
 * Creates a single Box element which is used by a Board. Holds states to show if and which
 * Player has 'won' the Box.
 *
 * @property state holds the state of the box
 *
 */
open class Box {

    protected var state: String = State.Empty.name

    fun getBoxState(): String {
        return this.state
    }

    /**
     * Sets a 'winner' for the box and changes its state to a respective player
     *
     * @param player1 true if player1 'won' the box or false if player2 takes it
     */
    fun setWinner(player1: Boolean) {
        if(player1) {
            this.state = State.Player1.name
        }
        else {
            this.state = State.Player2.name
        }
    }

    fun reset() { this.state = State.Empty.name }

    /**
     * Sets the states a box can have and con be used by any class to check in which state
     * a box is
     */
    companion object {
        /**
         *  Empty -> the box isn't won by anybody
         *  Player1 -> the first player wins the box
         *  Player2 -> the second player wind the box
         *  Draw -> no player wins the box, this status shouldn't be used by a simple box
         *  element, but rather by a board element that can be seen as a box at the same
         *  time, as its the case with an InnerBoard element
         *
         */
        enum class State {
            Empty, Player1, Player2, Draw
        }
    }
}
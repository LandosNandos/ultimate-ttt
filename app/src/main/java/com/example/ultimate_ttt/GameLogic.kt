package com.example.ultimate_ttt

import android.util.Log
import androidx.lifecycle.ViewModel

/**
 * Creates the Board and shows the state of the game to the grid fragments
 */
class GameLogic: ViewModel(){
    private val outerBoard = OuterBoard()
    private lateinit var moves : Moves
    private lateinit var innerBoard: InnerBoard
    private var lastClickedBoardIndex = 9

    /**
     * The 'second' Player is in this case another person that plays on the android devices of the
     * 'first' Player
     */
    fun simpleSecondPlayer() {
        this.moves = Moves()
    }

    /**
     * Is used when the second player is an AI or another person that plays on a different android
     * device
     */
    fun complexSecondPlayer() {
        val player2: SecondPlayer
    }

    /**
     * Returns the HaspMap Key of the InnerBoard that has been clicked last
     */
    fun getLastClickedBoardIndexStr(): String {
        return this.innerBoard.getIndex()
    }

    /**
     * Returns the index as a number of the InnerBoard that has been clicked last
     */
    fun getLastClickedBoardIndex(): Int {
        return lastClickedBoardIndex
    }

    fun setLastClickedBoardIndex(index: Int) {
        lastClickedBoardIndex = index
    }

    fun getCurrentTurn(): String {
        return moves.getCurrentTurn()
    }

    fun reset() {
        outerBoard.resetBoard()
    }

    /**
     * Returns the states of the boxes the innerBoard has as Strings
     */
    fun getInnerBoard(): MutableList<String> {
        val size = this.innerBoard.items.size
        val states = mutableListOf<String>().apply {
            for(i in 0 until size) {
                add(innerBoard.items.values.elementAt(i).getBoxState())
            }
        }
        return states
    }

    fun getOuterBoard(): OuterBoard {
        return this.outerBoard
    }

    /**
     * Should be used when a InnerBoard element is clicked in the view.
     *
     * @param key is the key value for the HashMap of InnerBoards the OuterBoard holds and is the
     * same as the Indexes for the InnerFragments in the View for example: "00", "01", "02",
     * "10", "11", etc.
     */
    fun onInnerBoardClick(key: String) : InnerBoard? {
        val inner = outerBoard.items[key] as InnerBoard
        if (inner != null) {
            this.innerBoard = inner!!
            return this.innerBoard
        } else {
            throw NullPointerException("Expression 'board.boards[index]' must not be null")
        }
    }

//    fun onInnerBoardClick(key: String): InnerBoard {
//        return outerBoard.boards[key] ?: throw NullPointerException("InnerBoard is null for key: $key")
//    }

    /**
     * Should be used when a Box element is clicked in the view
     *
     * @param key is the key value for the HashMap of Boxes a InnerBoard holds and is the
     * same as the Indexes for the InnerFragments in the View for example: "00", "01", "02",
     * "10", "11", etc.
     */
    fun onBoxClick(key: String): Boolean {
        val clickedBox = this.innerBoard.items[key]
        val processOuterBoard = moves.processTurnBox(this.innerBoard, clickedBox!!) ?: return false
        if(processOuterBoard) {
            if(moves.processTurnBox(outerBoard, null)!!) {
                Log.v("GameWinner", this.outerBoard.getBoxState())
            }
        }
        return true
    }
}
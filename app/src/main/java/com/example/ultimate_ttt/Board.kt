package com.example.ultimate_ttt

//Superclass
/**
 * Creates a board element which holds boxes. It implements the box case, so a board
 * element can also have states that show, which player has won it
 *
 * @property items holds the box elements in a Map. Must be implemented by the subclasses
 * of this class
 */
open class Board: Box() {

    lateinit var items: MutableMap<String, Box>

    /**
     * Counts how many boxes aren't empty and returns the number
     */
    fun getCount(): Int {
        var count = 0
        this.items.values.forEach{
            if(it.getBoxState() != Box.Companion.State.Empty.name) {
                count++
            }
        }
        return count
    }

    /**
     * Sets the board state to a draw. It's implemented here instead of inside the box class,
     * because a simple box element can't be set to 'draw'
     */
    fun draw() { this.state = Box.Companion.State.Draw.name }

}
//Subclass
/**
 * Creates a normal Tic-Tac-Toe field. Implements the board class and fills the items property with
 * an LinkedHashMap with 9 box elements.
 *
 * @param row a number from 0 to 2. Shows in which row of the ttt-field the box is
 * @param column a number from 0 to 2. Shows in which column of the ttt-field the box is
 */
class InnerBoard(private val row: Int, private val column: Int): Board() {

    init {
        items = mutableMapOf<String, Box>().apply {
            for(i in 0..2) {
                for(j in 0 ..2) {
                    put("${i%3}${j%3}", Box())
                }
            }
        }
    }

    fun getIndex(): String {
        return "${this.row}${this.column}"
    }

    fun resetBoard() {
        super.reset()
        items.forEach {it.value.reset()}
    }
}

//Subclass
/**
 * Create the Ultimate-Tic-Tac-Toe field. Implements the board class and fills the items property
 * with an LinkedHashMap with 9 InnerBoard elements.
 */
class OuterBoard: Board() {

    init {
        items =  mutableMapOf<String, Box>().apply {
            for(i in 0..2) {
                for(j in 0 ..2) {
                    put("${i%3}${j%3}", InnerBoard(i%3,j%3))
                }
            }
        }
    }

    fun resetBoard() {
        super.reset()
        items.forEach {it.value.reset()}
    }
}
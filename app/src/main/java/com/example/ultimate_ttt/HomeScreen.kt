package com.example.ultimate_ttt

import android.content.Intent
import android.os.Bundle
import android.widget.Button
import androidx.appcompat.app.AppCompatActivity

class HomeScreen : AppCompatActivity() {

    private lateinit var singleplayer: Button
    private lateinit var multiplayer: Button
    private lateinit var scoreboard: Button
    private lateinit var options: Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.home_screen)

        singleplayer = findViewById(R.id.SPButton)
        multiplayer = findViewById(R.id.MPButton)
        scoreboard = findViewById(R.id.SBButton)
        options = findViewById(R.id.OPTButton)

        singleplayer.setOnClickListener {
            val intent = Intent(this@HomeScreen, MainActivity::class.java)
            val yourString = "Singleplayer"
            intent.putExtra("selectedActivity", yourString)
            startActivity(intent)
        }

        multiplayer.setOnClickListener {
            val intent = Intent(this@HomeScreen, MainActivity::class.java)
            val yourString = "Multiplayer"
            intent.putExtra("selectedActivity", yourString)
            startActivity(intent)
        }
    }
}